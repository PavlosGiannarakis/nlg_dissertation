package bigram;

public class BigramModelDemo {
	public static void main(String[] args) {
				
		BigramModel bim = new BigramModel();
		
		bim.perplexityOf("something nice");
		//bim.perplexityOf("Jeremy Corbyn Jeremy Corbyn");
		//bim.perplexityOf("Jeremy Corbin");
		//bim.perplexityOf("Jeremy Corbyn is the new Labour party leader");
		//bim.perplexityOf("bananas and cream");
		//bim.perplexityOf("bananas and cream bananas and cream");
		//bim.perplexityOf("In a 15-minute speech, he did not mention");
		
		//bim.playShannonGame();
		/**
		//crossword clues from i
		bim.perplexityOf("Stern violinist");
		bim.perplexityOf("Ste violi");
		bim.perplexityOf("Lead top off a beer");
		bim.perplexityOf("Beethoven stops play at the oval for intellect");
		bim.perplexityOf("Belgium by downpour is intellect");
		bim.perplexityOf("long walk yields temperature by slope");
		bim.perplexityOf("maiden  finishes  repairs");
		bim.perplexityOf("repairs and French man by finishes");
		**/
		//bim.bulkLoadClues();
		
		/**
		//clues from http://www.alberichcrosswords.com/pages/xword80.html
		bim.perplexityOf("Walk endlessly, wearing fancy footwear");
		bim.perplexityOf("Decline job, being somewhat upset");
		bim.perplexityOf("Hesitant policeman has to attempt arresting the man");
		bim.perplexityOf("Noodle is sweet");
		
		
		//clues taken from http://www.theguardian.com/crosswords/cryptic/26427
		bim.perplexityOf("Healthy source of water");
		bim.perplexityOf("Cross among religious houses");
		bim.perplexityOf("With second replacement hip");
		bim.perplexityOf("Sit back alongside a storyteller");
		*/
		
		/**
		bim.perplexityOf("I am");
		bim.perplexityOf("I am green");
		bim.perplexityOf("I am red");
		*/
		
		
		//bim.perplexityOf("I am Sam");
		//bim.perplexityOf("Sam I am");
		//bim.perplexityOf("I am eggs and ham");
		//bim.perplexityOf("Chocolate and chips eggs and ham");
		//bim.perplexityOf("Potatoes and chips");
		//bim.perplexityOf("phone box snake television");
		//bim.perplexityOf("pho bo snak televi");
			

		
		

		
		
	}
}
