package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import trigram.TrigramModelMod;

//Main runnable class of the project, used to run the algorithms and display the results.
public class Dissertation_Demo {

	public static void main(String[] args) {
		long startTime1, startTime2, startTime3;
		long endTime1, endTime2, endTime3;
		TrigramModelMod tgm = new TrigramModelMod();
		int top = 10;
		ArrayList<String> evaluation = new ArrayList<String>();;
		String timingsOutput = "";
		
		//Reads evaluations words and adds them to an array list which is then used to feed the algorithms sequentially.
		try{
			Scanner s = new Scanner(new File("evaluation.txt"));
			while (s.hasNext()){
				evaluation.add(s.next().toLowerCase());
			}
			s.close();
		}catch(FileNotFoundException e)
		{
			System.out.println("Not found");
		}
		
		//For each answer get the results, time them and then print the results to file. The files are individually named so won't be overwritten.
		System.out.println("Starting to get results...");
		for(String answer: evaluation)
		{
			Algorithm alg1 = new Algorithm(answer, top, tgm);
			startTime1 = System.nanoTime();
			alg1.run();
			endTime1 = System.nanoTime();
			double duration1 = (endTime1 - startTime1) / 1000000000.0;
			
			Algorithm2 alg2 = new Algorithm2(answer, top, tgm); 
			startTime2 = System.nanoTime();
			alg2.run();
			endTime2 = System.nanoTime();
			double duration2 = (endTime2 - startTime2) / 1000000000.0;
			
			CleverBruteForce bf = new CleverBruteForce(answer, top, tgm);
			double duration3 = 0.0;
			try{
				startTime3 = System.nanoTime();
				bf.run();
				endTime3 = System.nanoTime();
				duration3 = (endTime3 - startTime3) / 1000000000.0;
	
			} catch(Exception e) {
				e.printStackTrace();
			}
			timingsOutput += answer + "\n" + "1: " + duration1 + "\n" + "2: " + duration2 + "\n" + "BF: " + duration3 + "\n" + "-------------------------\n";
			alg1.saveAndPrint();
			alg2.saveAndPrint();
			bf.saveAndPrint();
		}
		
    	try {
		    PrintWriter pw = new PrintWriter(new FileOutputStream("timings.txt"));
		    pw.println(timingsOutput);
		    pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Finished calculating results.");
	}
}
