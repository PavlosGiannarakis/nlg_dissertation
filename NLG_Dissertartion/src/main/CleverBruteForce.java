package main;

import dictionary.Dictionary;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.TreeSet;
import trigram.TrigramModelMod;
import trigram.RankedClue;

public class CleverBruteForce {
	//Instances of classes used as part of project
	InitialWords iw = new InitialWords();
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModelMod tgm;// = new TrigramModel();
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> topClues = new TreeSet <RankedClue> ();
	
	//Variables to store answer and number of clues needed as provided by the Constructor.
	private String answer;
	private int top;
	
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl;
	
	//Constructor sets parameters
	public CleverBruteForce(String ans, int limit, TrigramModelMod t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results.
	public void run()
	{
		getAnswerBreakdown(answer);
		getBestCombinations();
	}
	
	//Saves top clues to a file and the prints them.
	public void saveAndPrint()
	{
		saveTopClues();
		//printTopClues();
	}
	
	//Write results to file with the specific name that shows the test conditions.
	private void saveTopClues()
	{
    	try {
		    PrintWriter pw = new PrintWriter(new FileOutputStream("bf_"+ answer +"_mediumData_SW_SP"+ ".txt"));
		    for (RankedClue rl : topClues)
		        pw.println(rl.getPerplexity() + " " + rl.getText());
		    pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void printTopClues()
	{
		System.out.println("Brute force:");
		for(RankedClue rl : topClues)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	//Method to return top N combinations.
	private void getBestCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		int i = 0;
		boolean flag = false;
		
		for(String iw_t : iw.getAllInitialwords()) {
			for(String def_t : dict.getAllWordsStartingWith(firstLetter))
			{
				if(def_t.length() > 3) // too high? 
				{
					try 
					{
						for(String def2_t : dict.getAllDefinitions(wordMinusFl)) {
							for(String lnk: drl.getAllLinkwords())
							{
								try {
									for(String fw: dict.getAllDefinitions(answer)) {
										if(lnk != "")
										{
											example = iw_t + " " + def_t + " " + def2_t + " " + lnk + " " + fw;
										} else example = iw_t + " " + def_t + " " + def2_t + " " + fw;
										
										perplexity = tgm.perplexityOf(example);
										aRankedClue = new RankedClue(perplexity, example);
										if(i < top)
										{
											topClues.add(aRankedClue);
											i++;
										}
										else if(topClues.last().getPerplexity() > perplexity) {
											topClues.pollLast();
											topClues.add(aRankedClue);
										}
										
									}
								}
								catch(NullPointerException e){
									System.out.println(answer + " doesn't have any defined definitions.");
									flag = true;
									break;
								}
							}
							if (flag == true) break;
						}
						if (flag == true) break;
					}
					catch(NullPointerException e){
						System.out.println(wordMinusFl + " doesn't have any defined definitions.");
						flag = true;
						break;
					}
				}
				if (flag == true) break;
			}
			if (flag == true) break;
		}
	}
}
