package main;

import java.util.ArrayList;
import java.util.Random;

public class InitialWords {

	private ArrayList <String> initialWords;
	
	//Constructor that initialises an ArrayList and then loads all the options. 
	public InitialWords() {
		this.initialWords = new ArrayList <String> ();
		loadInitialWords();
	}
	
	public ArrayList <String> getAllInitialwords() {
		return this.initialWords;
	}
	
	public String getOneRandomInitialword() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(this.initialWords.size());
		return this.initialWords.get(randomInt);
	}
	
	public String getInitialWordAt(int position)
	{
		return initialWords.get(position);
	}
	
	//Load all Initial Words
	private void loadInitialWords() {
		//this.initialWords.add("Head");?
		this.initialWords.add("Heads");
		this.initialWords.add("Head to");
		this.initialWords.add("Head of");
		this.initialWords.add("Starts");
		this.initialWords.add("Tips");
		this.initialWords.add("Starters");
		this.initialWords.add("Open");
		this.initialWords.add("Openers");
		this.initialWords.add("Primary");
		this.initialWords.add("Primarily");
		this.initialWords.add("Begin");
		this.initialWords.add("Begin to");
		this.initialWords.add("Beginning");
		this.initialWords.add("Beginning of");
		this.initialWords.add("Beginning to");
		this.initialWords.add("First");
		this.initialWords.add("First of");
		this.initialWords.add("First to");
		this.initialWords.add("Initial");
		this.initialWords.add("Initially");
		this.initialWords.add("Start");
		this.initialWords.add("Start of");
		this.initialWords.add("Start to");
	}
}
