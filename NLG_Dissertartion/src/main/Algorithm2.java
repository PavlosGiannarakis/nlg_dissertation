package main;

import dictionary.Dictionary;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TreeSet;
import trigram.TrigramModelMod;
import trigram.RankedClue; // Only one imported as Trigram uses the same code.


public class Algorithm2 {
	//Instances of classes used as part of project
	InitialWords iw = new InitialWords();
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModelMod tgm;
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> allStartingClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> linkingWordClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> fullWordClues = new TreeSet <RankedClue> ();
	
	//Variables to store answer and number of clues needed as provided by the Constructor.
	private String answer;
	private int top;
	
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl, temp_clue;
	
	//New ArrayList to store top ten.
	private ArrayList<String> topClues = new ArrayList<String>();
	private TreeSet <RankedClue> topCluesAndPerplexity = new TreeSet <RankedClue> ();
	
	//Constructor sets parameters
	public Algorithm2(String ans, int limit, TrigramModelMod t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results.
	public void run()
	{
		getAnswerBreakdown(answer);
		topClues = getBestStartingCombinations();
		RankedClue arc;
		
		if(topClues.size() > 0)
		{
			for(int i = 0; i < top; i++)
			{
				temp_clue = "";
				temp_clue = topClues.get(i);
				temp_clue += " " + getLinkingWord(temp_clue);
				temp_clue = temp_clue.trim();
				try 
				{
					temp_clue += " " + getDefinitionFullWord(temp_clue);
				}
				catch(NullPointerException e){
					System.out.println(answer + " doesn't have any defined definitions.");
					break;
				}
				
				double perplexity = tgm.perplexityOf(temp_clue);
				arc = new RankedClue(perplexity,temp_clue);
				topCluesAndPerplexity.add(arc);
			}
		}
	}
	
	//Write top N answers to a file and then print them to console.
	public void saveAndPrint()
	{
		saveTopClues();
		//printTopClues();
	}
	
	//Write results to file with the specific name that shows the test conditions.
	private void saveTopClues()
	{
    	try {
		    PrintWriter pw = new PrintWriter(new FileOutputStream("alg2_"+ answer +"_mediumData_SW_SP"+ ".txt"));
		    for (RankedClue rl : topCluesAndPerplexity)
		        pw.println(rl.getPerplexity() + " " + rl.getText());
		    pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void printTopClues()
	{
		System.out.println("Algorithm 2:");
		for(RankedClue rl : topCluesAndPerplexity)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	private ArrayList<String> getBestStartingCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		boolean flag = false;
		
		for(String iw_t : iw.getAllInitialwords()) {
			for(String def_t : dict.getAllWordsStartingWith(firstLetter))
			{
				if(def_t.length() > 3) // too high? 
				{
					try {
						for(String def2_t : dict.getAllDefinitions(wordMinusFl)) {
							example = iw_t + " " + def_t + " " + def2_t;
							perplexity = tgm.perplexityOf(example);
							aRankedClue = new RankedClue(perplexity, example);
							allStartingClues.add(aRankedClue);
						}
					}
					catch(NullPointerException e){
						System.out.println(wordMinusFl + " doesn't have any defined definitions.");
						flag = true;
						break;
					}
				}
			}
			if(flag == true) break;
		}

	    int i = 0; 

	    ArrayList<String> res = new ArrayList<String>();
	     
	    for(RankedClue rl : allStartingClues) {
	    	if(i < top)
	    	{
		    	res.add(rl.getText());
	    	}
	    	else break;
	    	i++;
	    }
		return res;
	}

	private String getLinkingWord(String c) {
		linkingWordClues.clear();
		String temp = c.substring(c.indexOf(' ') + 1);
		String example;
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: drl.getAllLinkwords())
		{
			if(s != ""){
				example = temp + " " + s;
			} else example = temp;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			linkingWordClues.add(aRankedClue);
		}
		
		return linkingWordClues.first().getText();
	}
	
	private String getDefinitionFullWord(String c)
	{
		fullWordClues.clear();
		String example;
		RankedClue aRankedClue;
		double perplexity;
		for(String s: dict.getAllDefinitions(answer))
		{
			//example = temp + " " + s;
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			//System.out.println(example + " " + perplexity);
			fullWordClues.add(aRankedClue);
		}
		
		return fullWordClues.first().getText();
	}
	
}
