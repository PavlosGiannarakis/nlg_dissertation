package main;

import dictionary.Dictionary;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TreeSet;
import trigram.TrigramModelMod;
import trigram.RankedClue;


public class Algorithm {
	//Instances of classes used as part of project
	InitialWords iw = new InitialWords();
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModelMod tgm;
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> allStartingClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> shortWordClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> linkingWordClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> fullWordClues = new TreeSet <RankedClue> ();
	
	//Variables to store answer and number of clues needed as provided by the Constructor.
	private String answer;
	private int top;
	
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl, temp_clue;
	
	//New ArrayList to store top ten.
	private ArrayList<String> topClues = new ArrayList<String>();
	private TreeSet <RankedClue> topCluesAndPerplexity = new TreeSet <RankedClue> ();

	//Constructor sets parameters
	public Algorithm(String ans, int limit, TrigramModelMod t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results.
	public void run()
	{
		getAnswerBreakdown(answer);
		topClues = getBestStartingCombinations();
		RankedClue arc;
		for(int i = 0; i < top; i++)
		{
			temp_clue = ""; // ?
			temp_clue = topClues.get(i);
			
			try 
			{
				temp_clue += " " + getDefinitionShortWord(temp_clue);
			}
			catch(NullPointerException e){
				System.out.println(wordMinusFl + " doesn't have any defined definitions.");
				break;
			}
			
			temp_clue += " " + getLinkingWord(temp_clue);
			temp_clue = temp_clue.trim();
			try 
			{
				temp_clue += " " + getDefinitionFullWord(temp_clue);
			}
			catch(NullPointerException e){
				System.out.println(answer + " doesn't have any defined definitions.");
				break;
			}
			
			double perplexity = tgm.perplexityOf(temp_clue);
			arc = new RankedClue(perplexity,temp_clue);
			topCluesAndPerplexity.add(arc);
		}
	}
	
	//Writes to file and prints the top clues
	public void saveAndPrint()
	{
		saveTopClues();
		//No longer requires printing as no longer in testing
		//printTopClues();
	}
	
	//Write results to file with the specific name that shows the test conditions.
	private void saveTopClues()
	{
    	try {
		    PrintWriter pw = new PrintWriter(new FileOutputStream("alg1_"+ answer +"_mediumData_SW_SP"+ ".txt"));
		    for (RankedClue rl : topCluesAndPerplexity)
		        pw.println(rl.getPerplexity() + " " + rl.getText());
		    pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void printTopClues()
	{
		System.out.println("Algorithm 1");
		for(RankedClue rl : topCluesAndPerplexity)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	private ArrayList<String> getBestStartingCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		for(String iw_t : iw.getAllInitialwords()) {
			for(String def_t : dict.getAllWordsStartingWith(firstLetter))
			{
				if(def_t.length() > 3) // too high? 
				{
					example = iw_t + " " + def_t;
					perplexity = tgm.perplexityOf(example);
					aRankedClue = new RankedClue(perplexity, example);
					allStartingClues.add(aRankedClue);
				}
			}
		}

	    int i = 0; 

	    ArrayList<String> res = new ArrayList<String>();
	     
	    for(RankedClue rl : allStartingClues) {
	    	if(i < top)
	    	{
		    	res.add(rl.getText());
	    	}
	    	else break;
	    	i++;
	    }
		return res;
	}
	
	private String getDefinitionShortWord(String c)
	{
		shortWordClues.clear();
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: dict.getAllDefinitions(wordMinusFl))
		{
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			shortWordClues.add(aRankedClue);
		}
		
		return shortWordClues.first().getText();
	}

	private String getLinkingWord(String c) {
		linkingWordClues.clear();
		String temp = c.substring(c.indexOf(' ') + 1);
		String example;
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: drl.getAllLinkwords())
		{
			if(s != ""){
				example = temp + " " + s;
			} else example = temp;
			
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			linkingWordClues.add(aRankedClue);
		}
		
		return linkingWordClues.first().getText();
	}
	
	private String getDefinitionFullWord(String c)
	{
		fullWordClues.clear();
		String example;
		RankedClue aRankedClue;
		double perplexity;
		for(String s: dict.getAllDefinitions(answer))
		{
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			fullWordClues.add(aRankedClue);
		}
		
		return fullWordClues.first().getText();
	}
	
}
