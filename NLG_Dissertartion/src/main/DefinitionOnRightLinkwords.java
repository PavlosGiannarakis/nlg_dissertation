package main;

import java.util.ArrayList;
import java.util.Random;

public class DefinitionOnRightLinkwords {
	//Gives access to list of linking words
	private ArrayList <String> linkWords;
	boolean relaxALittle;
	
	public DefinitionOnRightLinkwords(boolean relax) {
		this.relaxALittle = relax;
		this.linkWords = new ArrayList <String> ();
		loadLinkWords();
	}
	
	public ArrayList <String> getAllLinkwords() {
		return this.linkWords;
	}
	
	public String getOneRandomLinkword() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(this.linkWords.size());
		return this.linkWords.get(randomInt);
	}
	
	public void printLinkwords() {
		String output = "";
		
		for(String tempIndicator : this.linkWords) {
			output = output + tempIndicator +"\n";
		}
		System.out.println(output);
		
	}

	private void loadLinkWords() {
		//Temporary removed as ngrams pick it up a lot
		
		
		this.linkWords.add("and");
		this.linkWords.add("because");//?
		this.linkWords.add("become");
		this.linkWords.add("becomes");
		this.linkWords.add("becoming");
		this.linkWords.add("being");//?
		this.linkWords.add("bringing");
		this.linkWords.add("brings");
		this.linkWords.add("bring");
		this.linkWords.add("by");//!
		this.linkWords.add("but");
		this.linkWords.add("creates");
		this.linkWords.add("creating");
		this.linkWords.add("delivers");
		this.linkWords.add("delivering");
		this.linkWords.add("finding");
		this.linkWords.add("finds");
		this.linkWords.add("for");
		this.linkWords.add("from");
		this.linkWords.add("gets");
		this.linkWords.add("getting");
		this.linkWords.add("gives");
		this.linkWords.add("giving");
		this.linkWords.add("into");
		this.linkWords.add("in");
		this.linkWords.add("is");
		this.linkWords.add("leading to");
		this.linkWords.add("lead to");
		this.linkWords.add("leads to");
		this.linkWords.add("makes");
		this.linkWords.add("making");
		this.linkWords.add("or");
		this.linkWords.add("of");
		this.linkWords.add("offers");
		this.linkWords.add("offering");
		this.linkWords.add("produced");
		this.linkWords.add("produces"); 
		this.linkWords.add("producing");
		this.linkWords.add("provides");
		this.linkWords.add("providing");
		this.linkWords.add("results to");
		this.linkWords.add("results in");
		this.linkWords.add("resulted in");
		this.linkWords.add("resulting in");
		this.linkWords.add("so");
		this.linkWords.add("showing");
		this.linkWords.add("shows");
		this.linkWords.add("show");//?
		this.linkWords.add("that's");
		this.linkWords.add("that gives");
		this.linkWords.add("that is");
		this.linkWords.add("that makes");
		this.linkWords.add("there's");
		this.linkWords.add("there are");
		this.linkWords.add("there is");
		this.linkWords.add("to");
		this.linkWords.add("to find");
		this.linkWords.add("to get");
		this.linkWords.add("to make");
		this.linkWords.add("to produce");
		this.linkWords.add("to see");//?
		this.linkWords.add("to reveal");
		//this.linkWords.add("");//?

		
		/** Non Ximenean */
		if(this.relaxALittle) {
			this.linkWords.add("and a");
			this.linkWords.add("for a");
			this.linkWords.add("gets a");
			this.linkWords.add("gives a");
			this.linkWords.add("giving a");
			this.linkWords.add("into a");
			this.linkWords.add("is a");
			this.linkWords.add("leading to a");
			this.linkWords.add("leads to a");
			this.linkWords.add("makes a");
			this.linkWords.add("making a");
			this.linkWords.add("or a");
			this.linkWords.add("offering a");
			this.linkWords.add("produces a");
			this.linkWords.add("provides a");
			this.linkWords.add("to get a");
			this.linkWords.add("to get an");
			this.linkWords.add("to get to");
			this.linkWords.add("produced by");
			this.linkWords.add("out of");
			this.linkWords.add("out from");
			this.linkWords.add("at");
			this.linkWords.add("on");
		}

		
	}

}
