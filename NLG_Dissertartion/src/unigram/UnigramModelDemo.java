package unigram;

import java.util.ArrayList;
import java.util.Arrays;

public class UnigramModelDemo {
	public static void main(String[] args) {
		
		
		UnigramModel um = new UnigramModel();
		
		um.createModel();
		um.calculateProbability();
		um.testDistributionIsZero();
		
		//String sentence = "The sentence was very ffff";
		//String [] words = sentence.split(" ");
		
		//um.scoreFor(new ArrayList <String> (Arrays.asList(words)));
		
		/*sentence = "The sentence was very short";
		words = sentence.split(" ");*/

		//sentence = "Sam I am";
		//words = sentence.split(" "); // What do the scores mean? Good or Bad?
		
		//um.scoreFor(new ArrayList <String> (Arrays.asList(words)));
		
		//um.randomSentence(5);
		//um.randomSentence(7);
		
		//um.printFrequency();
	}
}
