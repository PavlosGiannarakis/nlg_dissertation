package main;

import dictionary.Dictionary;
import unigram.UnigramModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import bigram.BigramModel;
import trigram.TrigramModel;
import bigram.RankedClue; // Only one imported as Trigram uses the same code.


public class Algorithm4 {
	//Instances of classes used as part of project
	InitialWords iw = new InitialWords();
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	
	//BigramModel bgm = new BigramModel();
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModel tgm;// = new TrigramModel();
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> allStartingClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> shortWordClues = new TreeSet <RankedClue> ();
	
	//Temp variable for answer, will be taken from Demo file
	private String answer = "cash";
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl, temp_clue;
	//Variable to store selected starting combination of words
	//private String startWith;
	
	//New ArrayList to store top ten.
	private ArrayList<String> topClues = new ArrayList<String>();
	private TreeSet <RankedClue> topCluesAndPerplexity = new TreeSet <RankedClue> ();
	//Used to get N top clues.
	private int top = 30;
	
	//Constructor sets parameters
	public Algorithm4(String ans, int limit, TrigramModel t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results / maybe should also be written to file.
	public void run()
	{
		getAnswerBreakdown(answer);
		topClues = getBestStartingCombinations();
		RankedClue arc;
		for(int i = 0; i < top; i++)
		{
			temp_clue = topClues.get(i);
			temp_clue += " " + getInitializer(temp_clue); 
			temp_clue += " " + getDefinitionFirstWord(temp_clue);
			temp_clue += " " + getDefinitionShortWord(temp_clue);
			
			double perplexity = tgm.perplexityOf(temp_clue);
			arc = new RankedClue(perplexity,temp_clue);
			topCluesAndPerplexity.add(arc);
		}
		printTopClues();
	}
	
	private void printTopClues()
	{
		for(RankedClue rl : topCluesAndPerplexity)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	private ArrayList<String> getBestStartingCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		for(String def_t : dict.getAllDefinitions(answer)) {
			for(String link_t : drl.getAllLinkwords())
			{
				example = def_t + " " + link_t;
				perplexity = tgm.perplexityOf(example);
				aRankedClue = new RankedClue(perplexity, example);
				allStartingClues.add(aRankedClue);
			}
		}

	    int i = 0; 

	    ArrayList<String> res = new ArrayList<String>();
	     
	    for(RankedClue rl : allStartingClues) {
	    	if(i < top)
	    	{
		    	res.add(rl.getText());
	    	}
	    	else break;
	    	i++;
	    }
		return res;
	}
	
	private String getDefinitionShortWord(String c)
	{
		shortWordClues.clear();
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: dict.getAllDefinitions(wordMinusFl))
		{
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			shortWordClues.add(aRankedClue);
		}
		
		return shortWordClues.first().getText();
	}
	
	private String getDefinitionFirstWord(String c)
	{
		shortWordClues.clear();
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: dict.getAllWordsStartingWith(firstLetter))
		{
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			shortWordClues.add(aRankedClue);
		}
		
		return shortWordClues.first().getText();
	}
	
	private String getInitializer(String c)
	{
		shortWordClues.clear();
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: iw.getAllInitialwords())
		{
			example = c + " " + s.toLowerCase();
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			shortWordClues.add(aRankedClue);
		}
		
		return shortWordClues.first().getText();
	}
}
