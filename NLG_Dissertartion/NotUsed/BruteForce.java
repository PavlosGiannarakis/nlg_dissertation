package main;

import dictionary.Dictionary;
import java.util.TreeSet;
import trigram.TrigramModel;
import bigram.RankedClue; // Only one imported as Trigram uses the same code.


public class BruteForce {
	//Instances of classes used as part of project
	InitialWords iw = new InitialWords();
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModel tgm;// = new TrigramModel();
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> allFullClues = new TreeSet <RankedClue> ();
	
	//Temp variable for answer, will be taken from Demo file
	private String answer;
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl;
	//Used to get N top clues.
	private int top;
	
	//Constructor sets parameters
	public BruteForce(String ans, int limit, TrigramModel t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results / maybe should also be written to file.
	public void run()
	{
		getAnswerBreakdown(answer);
		getBestCombinations();
		printTopClues();
	}
	
	private void printTopClues()
	{
		int i = 0;
		for(RankedClue rl : allFullClues)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
			i++;
			if(i == top) break;
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	private void getBestCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		for(String iw_t : iw.getAllInitialwords()) {
			for(String def_t : dict.getAllWordsStartingWith(firstLetter))
			{
				if(def_t.length() > 3) // too high? 
				{
					for(String def2_t : dict.getAllDefinitions(wordMinusFl)) {
						for(String lnk: drl.getAllLinkwords())
						{
							for(String fw: dict.getAllDefinitions(answer)) {
								if(lnk != "")
								{
									example = iw_t + " " + def_t + " " + def2_t + " " + lnk + " " + fw;
								} else example = iw_t + " " + def_t + " " + def2_t + " " + fw;
								
								perplexity = tgm.perplexityOf(example);
								aRankedClue = new RankedClue(perplexity, example);
								allFullClues.add(aRankedClue);
							}
						}
					}
				}
			}
		}
	}
}
