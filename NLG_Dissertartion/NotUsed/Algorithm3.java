package main;

import dictionary.Dictionary;
import unigram.UnigramModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import bigram.BigramModel;
import trigram.TrigramModel;
import bigram.RankedClue; // Only one imported as Trigram uses the same code.


public class Algorithm3 {
	//Instances of classes used as part of project
	DefinitionOnRightLinkwords drl = new DefinitionOnRightLinkwords(false); //Set to false as we want Ximerian answers.
	Dictionary dict = new Dictionary();
	
	//Trigram Model used, it incorporates the bigrams and unigrams as well.
	TrigramModel tgm;// = new TrigramModel();
	
	//TreeSet used with Ranked Clue to instantly organise the best option from the ones tested at each stage.
	private TreeSet <RankedClue> allStartingClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> linkingWordClues = new TreeSet <RankedClue> ();
	private TreeSet <RankedClue> fullWordClues = new TreeSet <RankedClue> ();
	
	//Temp variable for answer, will be taken from Demo file
	private String answer = "";
	
	//Vital variables required to store the clue created.
	private String firstLetter, wordMinusFl, temp_clue;
	
	//New ArrayList to store top ten.
	private ArrayList<String> topClues = new ArrayList<String>();
	private TreeSet <RankedClue> topCluesAndPerplexity = new TreeSet <RankedClue> ();
	
	//Used to get N top clues.
	private int top = 30;
	
	//Constructor sets parameters
	public Algorithm3(String ans, int limit, TrigramModel t) {
		answer = ans;
		top = limit;
		tgm = t;
	}
	
	//Runs the algorithm to produce the results / maybe should also be written to file.
	public void run()
	{
		getAnswerBreakdown(answer);
		topClues = getBestStartingCombinations();
		RankedClue arc;
		for(int i = 0; i < top; i++)
		{
			temp_clue = ""; // ?
			temp_clue = topClues.get(i);
			temp_clue += " " + getLinkingWord(temp_clue);
			temp_clue += " " + getDefinitionFullWord(temp_clue);
			
			double perplexity = tgm.perplexityOf(temp_clue);
			arc = new RankedClue(perplexity,temp_clue);
			topCluesAndPerplexity.add(arc);
		}
		printTopClues();
	}
	
	private void printTopClues()
	{
		for(RankedClue rl : topCluesAndPerplexity)
		{
			System.out.println(rl.getPerplexity() + " " + rl.getText());
		}
	}

	private void getAnswerBreakdown(String s)
	{
		firstLetter = Character.toString(s.charAt(0)).toLowerCase();
		wordMinusFl = s.substring(1, s.length());
	}
	
	
	private ArrayList<String> getBestStartingCombinations()
	{
		String example = "";
		RankedClue aRankedClue;
		double perplexity;
		
		for(String def_t : dict.getAllWordsStartingWith(firstLetter))
		{
				if(def_t.length() > 3) // too high? 
				{
					for(String def2_t : dict.getAllDefinitions(wordMinusFl)) 
					{
						example = def_t + " " + def2_t;
						perplexity = tgm.perplexityOf(example);
						aRankedClue = new RankedClue(perplexity, example);
						allStartingClues.add(aRankedClue);
					}
				}
		}

	    int i = 0; 

	    ArrayList<String> res = new ArrayList<String>();
	     
	    for(RankedClue rl : allStartingClues) {
	    	if(i < top)
	    	{
		    	res.add(rl.getText());
	    	}
	    	else break;
	    	i++;
	    }
		return res;
	}

	private String getLinkingWord(String c) {
		linkingWordClues.clear();
		String temp = c.substring(c.indexOf(' ') + 1);
		String example;
		RankedClue aRankedClue;
		double perplexity;
		
		for(String s: drl.getAllLinkwords())
		{
			example = temp + " " + s;			
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			linkingWordClues.add(aRankedClue);
		}
		
		return linkingWordClues.first().getText();
	}
	
	private String getDefinitionFullWord(String c)
	{
		fullWordClues.clear();
		String[] exampleAL = c.split(" ");
		String example;
		RankedClue aRankedClue;
		double perplexity;
		for(String s: dict.getAllDefinitions(answer))
		{
			example = c + " " + s;
			perplexity = tgm.perplexityOf(example);
			aRankedClue = new RankedClue(perplexity, s);
			fullWordClues.add(aRankedClue);
		}
		
		return fullWordClues.first().getText();
	}
	
}
